## Postgres

[Bitnami helm chart](https://github.com/bitnami/charts) is used for Postgres.

To install dependencies use the following command:
```shell
helm dependency update ./helm/cenm-postgres/
```

## Prod environment

Install StorageDriver for AWS EFS:
```shell
helm repo add aws-efs-csi-driver https://kubernetes-sigs.github.io/aws-efs-csi-driver/
helm install aws-efs-csi-driver aws-efs-csi-driver/aws-efs-csi-driver
```

## Local environment

Start minikube with a local registry:
```shell
minikube start --driver=virtualbox --memory 10240 --disk-size 50G
#minikube start --driver=virtualbox --memory 10240 --disk-size 50G --insecure-registry="local-registry:5000"

#SSH to minikube and create path for PersistentVolume
minikube ssh 'cd /data && sudo mkdir cenm-db-pv cenm-signer-pv cenm-idman-pv cenm-notary-pv cenm-nmap-pv corda-nodes-platform-node-pv corda-nodes-oracle-node-pv'
```

Create cluster in minikube:
```shell
#Create storage class, namespace and RBAC
kubectl apply -f cenm.yaml
kubectl config set-context $(kubectl config current-context) --namespace=cenm
#kubectl config set-context (kubectl config current-context) --namespace=cenm

#Generate and create secrets
chmod +x scripts/generate_certs.sh
./scripts/generate_certs.sh

#Login to AWS ECR
TOKEN=$(aws ecr get-login-password)
#set TOKEN (aws ecr get-login-password)

kubectl create secret docker-registry aws-ecr-secret \
 --docker-server=400712362531.dkr.ecr.eu-west-2.amazonaws.com \
 --docker-username=AWS \
 --docker-password="$TOKEN"

kubectl create secret generic cenm-db-tls-secret \
    --from-file=./certs/cenm-db.crt \
    --from-file=./certs/cenm-db.key \
    --from-file=./certs/root-ca.crt

kubectl create secret generic identity-manager-tls-secret \
    --from-file=./certs/identity-manager.crt \
    --from-file=./certs/identity-manager.pk8 \
    --from-file=./certs/root-ca.crt

kubectl create secret generic network-map-tls-secret \
    --from-file=./certs/network-map.crt \
    --from-file=./certs/network-map.pk8 \
    --from-file=./certs/root-ca.crt

kubectl create secret generic platform-node-db-tls-secret \
    --from-file=./certs/platform-node-db.crt \
    --from-file=./certs/platform-node-db.key \
    --from-file=./certs/root-ca.crt

kubectl create secret generic platform-node-tls-secret \
    --from-file=./certs/platform-node.crt \
    --from-file=./certs/platform-node.pk8 \
    --from-file=./certs/root-ca.crt

kubectl create secret generic oracle-node-db-tls-secret \
    --from-file=./certs/oracle-node-db.crt \
    --from-file=./certs/oracle-node-db.key \
    --from-file=./certs/root-ca.crt

kubectl create secret generic oracle-node-tls-secret \
    --from-file=./certs/oracle-node.crt \
    --from-file=./certs/oracle-node.pk8 \
    --from-file=./certs/root-ca.crt

kubectl create secret generic notary-node-db-tls-secret \
    --from-file=./certs/notary-node-db.crt \
    --from-file=./certs/notary-node-db.key \
    --from-file=./certs/root-ca.crt

kubectl create secret generic notary-node-tls-secret \
    --from-file=./certs/notary-node.crt \
    --from-file=./certs/notary-node.pk8 \
    --from-file=./certs/root-ca.crt

#Deploy Postgres
helm install cenm-postgres ./helm/cenm-postgres --set persistence.useLocalStorage=true 

#Resolve minikube public ip
MINIKUBE_IP=$(minikube ip)
#set MINIKUBE_IP (minikube ip)

# these helm charts trigger public IP allocation
helm install idman-ip ./helm/idman-ip
helm install notary-ip ./helm/notary-ip

#Install Signer
helm install signer ./helm/signer --set idmanPublicIP=$MINIKUBE_IP --set persistence.useLocalStorage=true

#Install Identity Manager
helm install idman ./helm/idman --set persistence.useLocalStorage=true

#Install Notary
helm install notary ./helm/notary --set notary-node.notaryPublicIP=$MINIKUBE_IP --set global.persistence.useLocalStorage=true

#Install Network Map
helm install nmap ./helm/nmap --set persistence.useLocalStorage=true

#Create Corda Node Services
helm install corda-node-ips ./helm/corda-node-ips

#Create Corda Nodes
helm install corda-nodes ./helm/corda-nodes \
    --set platform-node.nodePublicIP=$MINIKUBE_IP \
    --set oracle-node.nodePublicIP=$MINIKUBE_IP \
    --set global.persistence.useLocalStorage=true

helm install corda-node-clients ./helm/corda-node-clients \
    --set platform-client.corda.rpc.host=corda-nodes-platform-node --set platform-client.corda.rpc.port=10100 \
    --set global.persistence.useLocalStorage=true

helm install extras ./helm/extras \
    --set global.persistence.useLocalStorage=true
```

Debug:
```shell
#Show pods
kubectl get pods

#Show existing namespaces
kubectl get ns

#Monitor events
kubectl get events --sort-by=.metadata.creationTimestamp
kubectl get events --sort-by=.metadata.creationTimestamp --watch

#Check current context
kubectl config current-context && kubectl config view --minify --output 'jsonpath={..namespace}' && echo

#Lint helm chart
helm template helm/corda-node/charts/platform-node/

helm list

#Get kube-proxy logs
kubectl logs -n kube-system --selector 'k8s-app=kube-proxy'

kubectl get pods --all-namespaces -o wide

#Minikube
minikube ssh
```

Cleanup:
```shell
#Delete minikube
minikube stop && minikube delete

#Cleanup PVs and PVCs
kubectl delete pv bc-pv-volume
kubectl delete pvc pvc-cenm

#Cleanup secrets
kubectl delete secret cenm-db-tls-secret identity-manager-tls-secret network-map-tls-secret
```


