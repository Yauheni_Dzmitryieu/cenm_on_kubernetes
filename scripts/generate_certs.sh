#!/usr/bin/env sh

echo 'WARNING: Not for production usage, for production use AWS Certificate Manager'

echo 'Recreating certificates directory..'
sudo rm -rf certs
mkdir certs

####################################################################
#ROOT
####################################################################

echo 'Generating Root CA..'

#Generate certificate signing request (CSR) for Root CA
openssl req -new -nodes -text -out certs/root-ca.csr \
  -keyout certs/root-ca.key -subj "/CN=ea-ssl-root-ca/O=Universal Asset Sciences Ltd/OU=Every Asset/L=London/C=UK"
chmod og-rwx certs/root-ca.key

#Sign and generate Root CA certificate
openssl x509 -req -in certs/root-ca.csr -text \
  -signkey certs/root-ca.key -out certs/root-ca.crt

####################################################################
#LEAF
####################################################################

echo 'Generating leaf certificates..'

#Generate leaf certificate for CENM Postgres
openssl req -new -nodes -text -out certs/cenm-db.csr \
  -keyout certs/cenm-db.key -subj "/CN=cenm-db.internal/O=Universal Asset Sciences Ltd/OU=Every Asset/L=London/C=UK"
chmod og-rwx certs/cenm-db.key
openssl x509 -req -in certs/cenm-db.csr -text -days 365 \
  -CA certs/root-ca.crt -CAkey certs/root-ca.key -CAcreateserial \
  -out certs/cenm-db.crt

#Generate leaf certificate for Identity Manager
openssl req -new -nodes -text -out certs/identity-manager.csr \
  -keyout certs/identity-manager.key -subj "/CN=identity-manager.internal/O=Universal Asset Sciences Ltd/OU=Every Asset/L=London/C=UK"
chmod og-rwx certs/identity-manager.key
openssl x509 -req -in certs/identity-manager.csr -text -days 365 \
  -CA certs/root-ca.crt -CAkey certs/root-ca.key -CAcreateserial \
  -out certs/identity-manager.crt

#Generate leaf certificate for network-map
openssl req -new -nodes -text -out certs/network-map.csr \
  -keyout certs/network-map.key -subj "/CN=network-map.internal/O=Universal Asset Sciences Ltd/OU=Every Asset/L=London/C=UK"
chmod og-rwx certs/network-map.key
openssl x509 -req -in certs/network-map.csr -text -days 365 \
  -CA certs/root-ca.crt -CAkey certs/root-ca.key -CAcreateserial \
  -out certs/network-map.crt

#Generate leaf certificate for Platform Node Postgres
openssl req -new -nodes -text -out certs/platform-node-db.csr \
  -keyout certs/platform-node-db.key -subj "/CN=platform-node-db.internal/O=Universal Asset Sciences Ltd/OU=Every Asset/L=London/C=UK"
chmod og-rwx certs/platform-node-db.key
openssl x509 -req -in certs/platform-node-db.csr -text -days 365 \
  -CA certs/root-ca.crt -CAkey certs/root-ca.key -CAcreateserial \
  -out certs/platform-node-db.crt

#Generate leaf certificate for Platform Node
openssl req -new -nodes -text -out certs/platform-node.csr \
  -keyout certs/platform-node.key -subj "/CN=platform-node.internal/O=Universal Asset Sciences Ltd/OU=Every Asset/L=London/C=UK"
chmod og-rwx certs/platform-node.key
openssl x509 -req -in certs/platform-node.csr -text -days 365 \
  -CA certs/root-ca.crt -CAkey certs/root-ca.key -CAcreateserial \
  -out certs/platform-node.crt

#Generate leaf certificate for Oracle Node Postgres
openssl req -new -nodes -text -out certs/oracle-node-db.csr \
  -keyout certs/oracle-node-db.key -subj "/CN=oracle-node-db.internal/O=Universal Asset Sciences Ltd/OU=Every Asset/L=London/C=UK"
chmod og-rwx certs/oracle-node-db.key
openssl x509 -req -in certs/oracle-node-db.csr -text -days 365 \
  -CA certs/root-ca.crt -CAkey certs/root-ca.key -CAcreateserial \
  -out certs/oracle-node-db.crt

#Generate leaf certificate for Oracle Node
openssl req -new -nodes -text -out certs/oracle-node.csr \
  -keyout certs/oracle-node.key -subj "/CN=oracle-node.internal/O=Universal Asset Sciences Ltd/OU=Every Asset/L=London/C=UK"
chmod og-rwx certs/oracle-node.key
openssl x509 -req -in certs/oracle-node.csr -text -days 365 \
  -CA certs/root-ca.crt -CAkey certs/root-ca.key -CAcreateserial \
  -out certs/oracle-node.crt

#Generate leaf certificate for Notary Node Postgres
openssl req -new -nodes -text -out certs/notary-node-db.csr \
  -keyout certs/notary-node-db.key -subj "/CN=notary-node-db.internal/O=Universal Asset Sciences Ltd/OU=Every Asset/L=London/C=UK"
chmod og-rwx certs/notary-node-db.key
openssl x509 -req -in certs/notary-node-db.csr -text -days 365 \
  -CA certs/root-ca.crt -CAkey certs/root-ca.key -CAcreateserial \
  -out certs/notary-node-db.crt

#Generate leaf certificate for Notary Node
openssl req -new -nodes -text -out certs/notary-node.csr \
  -keyout certs/notary-node.key -subj "/CN=notary-node.internal/O=Universal Asset Sciences Ltd/OU=Every Asset/L=London/C=UK"
chmod og-rwx certs/notary-node.key
openssl x509 -req -in certs/notary-node.csr -text -days 365 \
  -CA certs/root-ca.crt -CAkey certs/root-ca.key -CAcreateserial \
  -out certs/notary-node.crt

echo 'Converting pem certificates for JDBC connections..'
openssl pkcs8 -topk8 -inform PEM -in certs/identity-manager.key -outform DER -nocrypt -out certs/identity-manager.pk8
openssl pkcs8 -topk8 -inform PEM -in certs/network-map.key -outform DER -nocrypt -out certs/network-map.pk8
openssl pkcs8 -topk8 -inform PEM -in certs/platform-node.key -outform DER -nocrypt -out certs/platform-node.pk8
openssl pkcs8 -topk8 -inform PEM -in certs/oracle-node.key -outform DER -nocrypt -out certs/oracle-node.pk8
openssl pkcs8 -topk8 -inform PEM -in certs/notary-node.key -outform DER -nocrypt -out certs/notary-node.pk8

echo 'Removing certificate signing requests..'
rm certs/*.csr

#Not required, is performed by k8s initContainers
#echo 'Adjusting permissions for certificates and certificate keys..'
#sudo chown 0:70 \
#  certs/cenm-db.ca-bundle certs/cenm-db.key \
#  certs/identity-manager.ca-bundle certs/identity-manager.key \
#  certs/network-map.ca-bundle certs/network-map.key \
#  certs/root-ca.crt
#
#sudo chown 0:0 \
#  certs/identity-manager.pk8 \
#  certs/network-map.pk8
#
#sudo chmod 640 \
#  certs/cenm-db.ca-bundle certs/cenm-db.key \
#  certs/identity-manager.ca-bundle certs/identity-manager.pk8 \
#  certs/network-map.ca-bundle certs/network-map.pk8
#
#sudo chmod 600 \
#  certs/identity-manager.key \
#  certs/network-map.key

echo 'SUCCESS'
