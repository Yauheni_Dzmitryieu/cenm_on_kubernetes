
variable "aws_region" {
  description = "AWS region"
  type = string
  default = "eu-west-1"
}

variable "instance_type" {
  description = "Instance type"
  type = string
  default = "t3.large"
}

variable "ec2_root_volume_size" {
  type    = string
  default = "30"
  description = "The volume size for the root volume in GiB"
}

variable "ami" {
  type = string
  default = "ami-0701e7be9b2a77600"
  description = "Default ubuntu image"
}

variable "prod-root-CA" {
  type = string
  default = "arn:aws:acm-pca:eu-west-1:675485732639:certificate-authority/73a4d6d0-f29a-48df-8865-7e29775e8e47"
  description = "Default arn for production root CA"
}
#################################################################################################################


variable "aws_availability_zone_A" {
  description = "AWS availability zone"
  type = string
  default = "eu-west-1a"
}

variable "aws_availability_zone_B" {
  description = "AWS availability zone"
  type = string
  default = "eu-west-1b"
}

variable "aws_availability_zone_C" {
  description = "AWS availability zone"
  type = string
  default = "eu-west-1c"
}
variable "main_cidrblock" {
  description = "AWS CIDR block"
  type = string
  default = "10.100.0.0/16"
}

variable "subnet_zone_B" {
  description = "AWS subnet in availability zone eu-west-1b"
  type = string
  default = "10.100.50.0/24"
}

variable "subnet_zone_C" {
  description = "AWS subnet in availability zone eu-west-1c"
  type = string
  default = "10.100.100.0/24"
}