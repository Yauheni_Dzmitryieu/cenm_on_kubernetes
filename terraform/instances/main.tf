//############################################################################
//#Set up instance  K8S  MASTER                                              #
//############################################################################
//resource "aws_instance" "master" {
//  instance_type = var.instance_type
//  ami = var.ami
//  vpc_security_group_ids = [ var.sg_K8S.id ]
//
//  key_name = aws_key_pair.kp-K8S-tr.key_name
//  subnet_id = var.subnet1.id
//  private_ip = "10.100.100.11"
//  root_block_device {
//    volume_size = var.ec2_root_volume_size
//    delete_on_termination = true
//  }
//  tags = {
//    Name = "K8Smaster-tr"
//  }
//  depends_on = [ var.gw_for_instance]
//}
//resource "aws_eip" "eip_master" {
//  instance = aws_instance.master.id
//  vpc = true
//}
//
//#######################################################################################
//#Set up instance worker1                                                              #
//#######################################################################################
//resource "aws_instance" "worker1" {
//  instance_type = var.instance_type
//  ami = var.ami
//  vpc_security_group_ids = [ var.sg_K8S.id]
//  key_name = aws_key_pair.kp-K8S-tr.key_name
//  subnet_id = var.subnet1.id
//  private_ip = "10.100.100.10"
//  root_block_device {
//    volume_size = var.ec2_root_volume_size
//    delete_on_termination = true
//  }
//  tags = {
//    Name = "worker1-tr"
//  }
//  depends_on = [ var.gw_for_instance ]
//}
//
//#######################################################################################
//#Set up instance worker2                                                              #
//#######################################################################################
//resource "aws_instance" "worker2" {
//  instance_type = var.instance_type
//  ami = var.ami
//  vpc_security_group_ids = [ var.sg_K8S.id ]
//  key_name = aws_key_pair.kp-K8S-tr.key_name
//  subnet_id = var.subnet1.id
//  private_ip = "10.100.100.20"
//  root_block_device {
//    volume_size = var.ec2_root_volume_size
//    delete_on_termination = true
//  }
//  tags = {
//    Name = "worker2-tr"
//  }
//  depends_on = [ var.gw_for_instance ]
//}