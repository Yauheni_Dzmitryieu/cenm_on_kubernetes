variable "aws_region" {
  description = "AWS region"
  type = string
  default = "eu-west-1"
}
variable "instance_type" {
  description = "Instance type for k8s"
  type = string
  default = "t3.large"
}

variable "ec2_root_volume_size" {
  type    = string
  default = "30"
  description = "The volume size for the root volume in GiB"
}

variable "ami" {
  type = string
  default = "ami-0701e7be9b2a77600"
  description = "Default ubuntu image for authentication service"
}
variable "sg_K8S" {
  type    = string
  description = "SG ID"
}
variable "subnet1" {
  type    = string
  description = "SUBNET ID"
}

variable "subnet2" {
  type    = string
  description = "SUBNET ID"
}

variable "gw_for_instance" {
  type    = string
  description = "NET GW"
}