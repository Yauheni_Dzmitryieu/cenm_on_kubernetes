//provider "aws" {
//  region = var.aws_region
//
//}
//module "network" {
//  source = "./network"
//
//}
//module "instances" {
//  source = "./instances"
//  security_group_id = module.network.SECURITY_GROUP
//  subnet_1_id = module.network.SUBNET1
//  subnet_2_id = module.network.SUBNET2
//  gw_for_instance = module.network.GW
//}
//
//module "loadbalances" {
//  source = "./loadbalances"
//  security_group_for_lbs = module.network.SECURITY_GROUP
//  instance_infra = module.instances.INSTANS-INFRA
//  target_id_keyclock = module.instances.TARGET_ID_KEYCLOCK
//  vpc_id = module.network.VPC_ID
//  subnet_1 = module.network.SUBNET1
//  subnet_2 = module.network.SUBNET2
//
//
//}
//
//module "route53" {
//  source = "./route53"
//  vpc_id = module.network.VPC_ID
//  pub_ip_lb_infra = module.instances.PUB_IP_LB_INFRA
//  pub_ip_lb_keyclock = module.instances.PUB_IP_LB_KEYKLOCK
//  dns_name_lb_infra = module.loadbalances.DNS_NAME_LB_INFRA
//  dns_name_lb_keyclock = module.loadbalances.DNS_NAME_LB_KEYCLOCK
//  dns_name_lb_auth-service = module.loadbalances.DNS_NAME_LB_AUT-SERVICE
//  zone_id_lb_infra = module.loadbalances.ZONE_ID_LB_INFRA
//  zone_id_lb_keyclock = module.loadbalances.ZONE_ID_LB_KEYCLOCK
//  zone_id_lb_auth-service = module.loadbalances.ZONE_ID_LB_AUT-SERVICE
//  hosted_zone_name = var.hosted_zone_name
//}