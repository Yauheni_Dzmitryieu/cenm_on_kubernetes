variable "security_group_for_lbs" {
  type = string
  description = "The security group for all LB"
}

variable "master" {
  type = string
  description = "The EC2 for k8s master node"
}

variable "subnet1" {
  type = string
  description ="The subnet for lb"
}
variable "subnet2" {
  type = string
  description ="The subnet for lb"
}

variable "main" {
  type = string
  description ="The VPC ID"
}
variable "certificate" {
  type = string
  description ="The certificate for lb, use after  DNS validation"
}

