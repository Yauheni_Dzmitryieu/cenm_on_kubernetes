//###############################################
//#     AWS LB for idman                        #
//###############################################
//resource "aws_lb" "lb_idman" {
//  name = "idman-lb-tr"
//  load_balancer_type = "network"
//  subnet_mapping {
//    subnet_id     = var.subnet1.id
//  }
//  subnet_mapping {
//    subnet_id     = var.subnet2.id
//  }
//  internal = false
//  enable_cross_zone_load_balancing = true
//  depends_on = [ var.master ]
//}
//
//resource "aws_lb_target_group" "target_group_idman" {
//  name = "idman-TG-tr"
//  port = 31000
//  protocol = "TCP"
//  target_type = "instance"
//  vpc_id = var.main.id
//
//  # TCP health check for api-server k8s
//  health_check {
//    protocol = "TCP"
//    port     = 6443
//
//    # NLBs required to use same healthy and unhealthy thresholds
//    healthy_threshold   = 3
//    unhealthy_threshold = 3
//
//    # Interval between health checks required to be 10 or 30
//    interval = 10
//  }
//}
//
//resource "aws_lb_target_group_attachment" "idman_31000" {
//  target_group_arn = aws_lb_target_group.target_group_idman.arn
//  target_id = var.master.id
//  port = 31000
//
//}
//
//resource "aws_lb_listener" "lb-idman_listener" {
//  load_balancer_arn = aws_lb.lb_idman.arn
//  port = "31000"
//  protocol = "TCP"
//
//  default_action {
//    type = "forward"
//    target_group_arn = aws_lb_target_group.target_group_idman.arn
//  }
//}
