provider "aws" {
  region = var.aws_region
  version = "3.11.0"
}
resource "aws_vpc" "main" {
  cidr_block = "10.100.0.0/16"
  enable_dns_hostnames = true
}
data "aws_availability_zone" "az_c" {
  name = "eu-west-1c"
}
resource "aws_subnet" "subnet1" {
  vpc_id = aws_vpc.main.id
  cidr_block = "10.100.100.0/24"
  availability_zone = "eu-west-1c"
  map_public_ip_on_launch = true

}

data "aws_availability_zone" "az_b" {
  name = "eu-west-1b"
}

resource "aws_subnet" "subnet2" {
  vpc_id = aws_vpc.main.id
  cidr_block = "10.100.50.0/24"
  availability_zone = "eu-west-1b"
  map_public_ip_on_launch = true
}

resource "aws_route_table" "rt" {
  vpc_id = aws_vpc.main.id
}
resource "aws_route" "r" {
  route_table_id = aws_route_table.rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.gw.id
}
## Association Public subnet with Route Table
resource "aws_route_table_association" "public" {
  subnet_id = aws_subnet.subnet1.id
  route_table_id = aws_route_table.rt.id
}
resource "aws_route_table_association" "public_subnet2" {
  subnet_id = aws_subnet.subnet2.id
  route_table_id = aws_route_table.rt.id
}


resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group" "sg_K8S" {
  name = "K8S"
  description = "Security Group for K8S"
  vpc_id = aws_vpc.main.id

  ingress {
    to_port = 22
    from_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    to_port = 443
    from_port = 443
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    to_port = 6443
    from_port = 6443
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    to_port = 10250
    from_port = 10250
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    to_port = 10255
    from_port = 10255
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    to_port = 8472
    from_port = 8472
    protocol = "udp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    to_port = 31000
    from_port = 31000
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    to_port = 31001
    from_port = 31001
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    to_port = 30301
    from_port = 30301
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    to_port = 30300
    from_port = 30300
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    to_port = 10000
    from_port = 10000
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    to_port = 80
    from_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  #ingress {
  #  to_port = 0
  #  from_port = 0
  #  protocol = "-1"
  #  cidr_blocks = ["0.0.0.0/0"]
  #}

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}


############################################################################
#Set up instance  K8S  MASTER                                              #
############################################################################
resource "aws_instance" "master" {
  instance_type = var.instance_type
  ami = var.ami
  vpc_security_group_ids = [
    aws_security_group.sg_K8S.id]

  key_name = aws_key_pair.kp-K8S-tr.key_name
  subnet_id = aws_subnet.subnet1.id
  private_ip = "10.100.100.11"
  root_block_device {
    volume_size = var.ec2_root_volume_size
    delete_on_termination = true
  }
  tags = {
    Name = "K8Smaster-tr"
  }
  depends_on = [
    aws_internet_gateway.gw]
}
resource "aws_eip" "eip_master" {
  instance = aws_instance.master.id
  vpc = true
}

#######################################################################################
#Set up instance worker1                                                              #
#######################################################################################
resource "aws_instance" "worker1" {
  instance_type = var.instance_type
  ami = var.ami
  vpc_security_group_ids = [
    aws_security_group.sg_K8S.id]
  key_name = aws_key_pair.kp-K8S-tr.key_name
  subnet_id = aws_subnet.subnet1.id
  private_ip = "10.100.100.10"
  root_block_device {
    volume_size = var.ec2_root_volume_size
    delete_on_termination = true
  }
  tags = {
    Name = "worker1-tr"
  }
  depends_on = [
    aws_internet_gateway.gw]
}

#######################################################################################
#Set up instance worker2                                                              #
#######################################################################################
resource "aws_instance" "worker2" {
  instance_type = var.instance_type
  ami = var.ami
  vpc_security_group_ids = [
    aws_security_group.sg_K8S.id]
  key_name = aws_key_pair.kp-K8S-tr.key_name
  subnet_id = aws_subnet.subnet1.id
  private_ip = "10.100.100.20"
  root_block_device {
    volume_size = var.ec2_root_volume_size
    delete_on_termination = true
  }
  tags = {
    Name = "worker2-tr"
  }
  depends_on = [
    aws_internet_gateway.gw]
}
###############################################
#     AWS LB for platform                     #
###############################################
resource "aws_lb" "lb_platform" {
  name               = "platform-lb-prod-tr"
  load_balancer_type = "application"
  security_groups    = [ aws_security_group.sg_K8S.id ]
  subnets            = [ aws_subnet.subnet2.id , aws_subnet.subnet1.id ]
  internal           = false
  depends_on = [ aws_instance.master ]
}

resource "aws_lb_target_group" "target_group_platform" {
  name     = "platform-prod-TG-tr"
  port     = 30300
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id
}

resource "aws_lb_target_group_attachment" "platform_30300" {
  target_group_arn = aws_lb_target_group.target_group_platform.arn
  target_id        = aws_instance.master.id
  port            = 30300
}

resource "aws_lb_listener" "lb-platform" {
  load_balancer_arn = aws_lb.lb_platform.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = aws_acm_certificate.cert_platform_ui_prod.arn


  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group_platform.arn
  }
}

resource "aws_lb_listener" "lb-platform-2" {
  load_balancer_arn = aws_lb.lb_platform.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
##############################################################################
#      Certificate for     platform.everyasset.net zone                      #
##############################################################################
data aws_route53_zone "public_root_domain" {
  name = "everyasset.net"
}

resource "aws_acm_certificate" "cert_platform_ui_prod" {
  domain_name               = "platform.everyasset.net"
  validation_method         = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_route53_record" "platform_ui_prod" {
  for_each = {
  for dvo in aws_acm_certificate.cert_platform_ui_prod.domain_validation_options : dvo.domain_name => {
    name   = dvo.resource_record_name
    record = dvo.resource_record_value
    type   = dvo.resource_record_type
  }
  }
  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.public_root_domain.id
}

resource "aws_acm_certificate_validation" "cert_platform_ui_validation" {
  certificate_arn         = aws_acm_certificate.cert_platform_ui_prod.arn
  validation_record_fqdns = [for record in aws_route53_record.platform_ui_prod : record.fqdn]
}

# Standard route53 DNS record for pointing to an ALB for platform service
resource "aws_route53_record" "pub_platform" {
  zone_id = "Z05875421HQ6UMML8UGK5"
  name    = "platform.everyasset.net"
  type    = "A"
  alias {
    name                   = aws_lb.lb_platform.dns_name
    zone_id                = aws_lb.lb_platform.zone_id
    evaluate_target_health = false
  }
}
##################################################################################
resource "aws_route53_record" "idman" {
  zone_id = "Z05875421HQ6UMML8UGK5"
  name    = "idman.everyasset.net"
  type    = "A"
  ttl     = "300"
  records = [  "54.216.87.230"  ]
}

resource "aws_route53_record" "nmap" {
  zone_id = "Z05875421HQ6UMML8UGK5"
  name    = "nmap.everyasset.net"
  type    = "A"
  ttl     = "300"
  records = [  "54.216.87.230"  ]
}
###############################################
#     AWS LB for SBA                          #
###############################################
resource "aws_lb" "lb_sba" {
  name               = "sba-prod-lb-tr"
  load_balancer_type = "application"
  security_groups    = [ aws_security_group.sg_K8S.id ]
  subnets            = [ aws_subnet.subnet2.id , aws_subnet.subnet1.id ]
  internal           = false
  depends_on = [ aws_instance.master ]
}

resource "aws_lb_target_group" "target_sba" {
  name     = "sba-prod-TG-tr"
  port     = 30301
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id
}

resource "aws_lb_target_group_attachment" "spring-boot_admin_30301" {
  target_group_arn = aws_lb_target_group.target_sba.arn
  target_id        = aws_instance.master.id
  port            = 30301
}

resource "aws_lb_listener" "lb_listener-spring-boot" {
  load_balancer_arn = aws_lb.lb_sba.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = aws_acm_certificate.cert_spring-boot-admin_prod.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_sba.arn
  }
}

resource "aws_lb_listener" "lb-listener_80" {
  load_balancer_arn = aws_lb.lb_sba.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
##############################################################################
#      Certificate for     sba.everyasset.net zone                           #
##############################################################################
resource "aws_acm_certificate" "cert_spring-boot-admin_prod" {
  domain_name               = "sba.everyasset.net"
  validation_method         = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "spring-boot_prod" {
  for_each = {
  for dvo in aws_acm_certificate.cert_spring-boot-admin_prod.domain_validation_options : dvo.domain_name => {
    name   = dvo.resource_record_name
    record = dvo.resource_record_value
    type   = dvo.resource_record_type
  }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.public_root_domain.id
}

resource "aws_acm_certificate_validation" "cert_spring-boot_validation" {
  certificate_arn         = aws_acm_certificate.cert_spring-boot-admin_prod.arn
  validation_record_fqdns = [for record in aws_route53_record.spring-boot_prod : record.fqdn]
}
#########################################################################################################
# Standard route53 DNS record for pointing to an ALB for sba service
resource "aws_route53_record" "pub_sba" {
  zone_id = "Z05875421HQ6UMML8UGK5"
  name    = "sba.everyasset.net"
  type    = "A"
  alias {
    name                   = aws_lb.lb_sba.dns_name
    zone_id                = aws_lb.lb_sba.zone_id
    evaluate_target_health = false
  }
}
################################################
#                  EFS                         #
################################################
#set rules for Signer EFS
resource "aws_efs_file_system" "signer-fs" {
  creation_token = "signer-fs-tr"
  performance_mode = "generalPurpose"
  tags = {
    Name = "signer-fs-tr"
  }
}
resource "aws_security_group" "ea-signer-efs-sg" {
  name = "Access_Signer_to_EFS"
  description = "Access from Signer to EFS"
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group_rule" "ea-signer-efs-rule" {
  security_group_id = aws_security_group.ea-signer-efs-sg.id
  type = "ingress"
  from_port = 2049
  to_port = 2049
  protocol = "tcp"
  source_security_group_id = aws_security_group.sg_K8S.id
}
resource "aws_efs_mount_target" "efs-signer-mnt" {
  file_system_id = aws_efs_file_system.signer-fs.id
  subnet_id = aws_subnet.subnet1.id
  security_groups = [
    aws_security_group.ea-signer-efs-sg.id]
}

#set rules for CENM External Database EFS
resource "aws_efs_file_system" "cenm-db-fs" {
  creation_token = "cenm-db-fs-tr"
  performance_mode = "generalPurpose"
  tags = {
    Name = "cenm-db-fs-tr"
  }
}
resource "aws_security_group" "ea-cenm-db-efs-sg" {
  name = "Access_CENM_db_to_EFS"
  description = "Access from CENM db to EFS"
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group_rule" "ea-cenm-db-efs-rule" {
  security_group_id = aws_security_group.ea-cenm-db-efs-sg.id
  type = "ingress"
  from_port = 2049
  to_port = 2049
  protocol = "tcp"
  source_security_group_id = aws_security_group.sg_K8S.id
}
resource "aws_efs_mount_target" "efs-cenm-db-mnt" {
  file_system_id = aws_efs_file_system.cenm-db-fs.id
  subnet_id = aws_subnet.subnet1.id
  security_groups = [
    aws_security_group.ea-cenm-db-efs-sg.id]
}

#set rules for Identity Manager EFS
resource "aws_efs_file_system" "identity-manager-fs" {
  creation_token = "identity-manager-fs-tr"
  performance_mode = "generalPurpose"
  tags = {
    Name = "identity-manager-fs-tr"
  }
}
resource "aws_security_group" "ea-identity-manager-efs-sg" {
  name = "Access_Identity_Manager_to_EFS"
  description = "Access from Identity Manager to EFS"
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group_rule" "ea-identity-manager-efs-rule" {
  security_group_id = aws_security_group.ea-identity-manager-efs-sg.id
  type = "ingress"
  from_port = 2049
  to_port = 2049
  protocol = "tcp"
  source_security_group_id = aws_security_group.sg_K8S.id
}
resource "aws_efs_mount_target" "efs-identity-manager-mnt" {
  file_system_id = aws_efs_file_system.identity-manager-fs.id
  subnet_id = aws_subnet.subnet1.id
  security_groups = [
    aws_security_group.ea-cenm-db-efs-sg.id]
}

#set rules for Network Map EFS
resource "aws_efs_file_system" "network-map-fs" {
  creation_token = "network-map-fs-tr"
  performance_mode = "generalPurpose"
  tags = {
    Name = "network-map-fs-tr"
  }
}
resource "aws_security_group" "ea-network-map-efs-sg" {
  name = "Access_Network_Map_to_EFS"
  description = "Access from Network Map to EFS"
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group_rule" "ea-network-map-efs-rule" {
  security_group_id = aws_security_group.ea-network-map-efs-sg.id
  type = "ingress"
  from_port = 2049
  to_port = 2049
  protocol = "tcp"
  source_security_group_id = aws_security_group.sg_K8S.id
}
resource "aws_efs_mount_target" "efs-network-map-mnt" {
  file_system_id = aws_efs_file_system.network-map-fs.id
  subnet_id = aws_subnet.subnet1.id
  security_groups = [
    aws_security_group.ea-network-map-efs-sg.id]
}

#set rules for Notary EFS
resource "aws_efs_file_system" "notary-node-fs" {
  creation_token = "notary-node-fs-tr"
  performance_mode = "generalPurpose"
  tags = {
    Name = "notary-node-fs-tr"
  }
}
resource "aws_security_group" "ea-notary-node-efs-sg" {
  name = "Access_Notary_Node_to_EFS"
  description = "Access from Notary Node to EFS"
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group_rule" "ea-notary-node-efs-rule" {
  security_group_id = aws_security_group.ea-notary-node-efs-sg.id
  type = "ingress"
  from_port = 2049
  to_port = 2049
  protocol = "tcp"
  source_security_group_id = aws_security_group.sg_K8S.id
}
resource "aws_efs_mount_target" "efs-notary-node-mnt" {
  file_system_id = aws_efs_file_system.notary-node-fs.id
  subnet_id = aws_subnet.subnet1.id
  security_groups = [
    aws_security_group.ea-notary-node-efs-sg.id]
}
resource "aws_efs_file_system" "notary-node-db-fs" {
  creation_token = "notary-node-db-fs-tr"
  performance_mode = "generalPurpose"
  tags = {
    Name = "notary-node-db-fs-tr"
  }
}
resource "aws_security_group" "ea-notary-node-db-efs-sg" {
  name = "Access_Notary_Node_DB_to_EFS"
  description = "Access from Notary Node DB to EFS"
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group_rule" "ea-notary-node-db-efs-rule" {
  security_group_id = aws_security_group.ea-notary-node-db-efs-sg.id
  type = "ingress"
  from_port = 2049
  to_port = 2049
  protocol = "tcp"
  source_security_group_id = aws_security_group.sg_K8S.id
}
resource "aws_efs_mount_target" "efs-notary-node-db-mnt" {
  file_system_id = aws_efs_file_system.notary-node-db-fs.id
  subnet_id = aws_subnet.subnet1.id
  security_groups = [
    aws_security_group.ea-notary-node-db-efs-sg.id]
}

#set rules for Corda Nodes EFS
resource "aws_efs_file_system" "platform-node-db-fs" {
  creation_token = "platform-node-db-fs-tr"
  performance_mode = "generalPurpose"
  tags = {
    Name = "platform-node-db-fs-tr"
  }
}
resource "aws_security_group" "ea-platform-node-db-efs-sg" {
  name = "Access_Platform_Node_DB_to_EFS"
  description = "Access from Platform Node DB to EFS"
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group_rule" "ea-platform-node-db-efs-rule" {
  security_group_id = aws_security_group.ea-platform-node-db-efs-sg.id
  type = "ingress"
  from_port = 2049
  to_port = 2049
  protocol = "tcp"
  source_security_group_id = aws_security_group.sg_K8S.id
}
resource "aws_efs_mount_target" "efs-platform-node-db-mnt" {
  file_system_id = aws_efs_file_system.platform-node-db-fs.id
  subnet_id = aws_subnet.subnet1.id
  security_groups = [
    aws_security_group.ea-platform-node-db-efs-sg.id]
}

resource "aws_efs_file_system" "platform-node-fs" {
  creation_token = "platform-node-fs-tr"
  performance_mode = "generalPurpose"
  tags = {
    Name = "platform-node-fs-tr"
  }
}
resource "aws_security_group" "ea-platform-node-efs-sg" {
  name = "Access_Platform_Node_to_EFS"
  description = "Access from Platform Node to EFS"
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group_rule" "ea-platform-node-efs-rule" {
  security_group_id = aws_security_group.ea-platform-node-efs-sg.id
  type = "ingress"
  from_port = 2049
  to_port = 2049
  protocol = "tcp"
  source_security_group_id = aws_security_group.sg_K8S.id
}
resource "aws_efs_mount_target" "efs-platform-node-mnt" {
  file_system_id = aws_efs_file_system.platform-node-fs.id
  subnet_id = aws_subnet.subnet1.id
  security_groups = [
    aws_security_group.ea-platform-node-efs-sg.id]
}

resource "aws_efs_file_system" "oracle-node-db-fs" {
  creation_token = "oracle-node-db-fs-tr"
  performance_mode = "generalPurpose"
  tags = {
    Name = "oracle-node-db-fs-tr"
  }
}
resource "aws_security_group" "ea-oracle-node-db-efs-sg" {
  name = "Access_Oracle_Node_DB_to_EFS"
  description = "Access from Oracle Node DB to EFS"
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group_rule" "ea-oracle-node-db-efs-rule" {
  security_group_id = aws_security_group.ea-oracle-node-db-efs-sg.id
  type = "ingress"
  from_port = 2049
  to_port = 2049
  protocol = "tcp"
  source_security_group_id = aws_security_group.sg_K8S.id
}
resource "aws_efs_mount_target" "efs-oracle-node-db-mnt" {
  file_system_id = aws_efs_file_system.oracle-node-db-fs.id
  subnet_id = aws_subnet.subnet1.id
  security_groups = [
    aws_security_group.ea-oracle-node-db-efs-sg.id]
}

resource "aws_efs_file_system" "oracle-node-fs" {
  creation_token = "oracle-node-fs-tr"
  performance_mode = "generalPurpose"
  tags = {
    Name = "oracle-node-fs-tr"
  }
}
resource "aws_security_group" "ea-oracle-node-efs-sg" {
  name = "Access_Oracle_Node_to_EFS"
  description = "Access from Oracle Node to EFS"
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group_rule" "ea-oracle-node-efs-rule" {
  security_group_id = aws_security_group.ea-oracle-node-efs-sg.id
  type = "ingress"
  from_port = 2049
  to_port = 2049
  protocol = "tcp"
  source_security_group_id = aws_security_group.sg_K8S.id
}
resource "aws_efs_mount_target" "efs-oracle-node-mnt" {
  file_system_id = aws_efs_file_system.oracle-node-fs.id
  subnet_id = aws_subnet.subnet1.id
  security_groups = [
    aws_security_group.ea-oracle-node-efs-sg.id]
}

#set rules for Extras EFS
resource "aws_efs_file_system" "artemis-master-fs" {
  creation_token = "artemis-master-fs-tr"
  performance_mode = "generalPurpose"
  tags = {
    Name = "artemis-master-fs-tr"
  }
}
resource "aws_security_group" "ea-artemis-master-efs-sg" {
  name = "Access_Artemis_Master_to_EFS"
  description = "Access from Artemis Master to EFS"
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group_rule" "ea-artemis-master-efs-rule" {
  security_group_id = aws_security_group.ea-artemis-master-efs-sg.id
  type = "ingress"
  from_port = 2049
  to_port = 2049
  protocol = "tcp"
  source_security_group_id = aws_security_group.sg_K8S.id
}
resource "aws_efs_mount_target" "efs-artemis-master-mnt" {
  file_system_id = aws_efs_file_system.artemis-master-fs.id
  subnet_id = aws_subnet.subnet1.id
  security_groups = [
    aws_security_group.ea-artemis-master-efs-sg.id]
}

resource "aws_efs_file_system" "artemis-slave-fs" {
  creation_token = "artemis-slave-fs-tr"
  performance_mode = "generalPurpose"
  tags = {
    Name = "artemis-slave-fs-tr"
  }
}
resource "aws_security_group" "ea-artemis-slave-efs-sg" {
  name = "Access_Artemis_Slave_to_EFS"
  description = "Access from Artemis Slave to EFS"
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group_rule" "ea-artemis-slave-efs-rule" {
  security_group_id = aws_security_group.ea-artemis-slave-efs-sg.id
  type = "ingress"
  from_port = 2049
  to_port = 2049
  protocol = "tcp"
  source_security_group_id = aws_security_group.sg_K8S.id
}
resource "aws_efs_mount_target" "efs-artemis-slave-mnt" {
  file_system_id = aws_efs_file_system.artemis-slave-fs.id
  subnet_id = aws_subnet.subnet1.id
  security_groups = [
    aws_security_group.ea-artemis-slave-efs-sg.id]
}

resource "aws_efs_file_system" "task-executor-fs" {
  creation_token = "task-executor-fs-tr"
  performance_mode = "generalPurpose"
  tags = {
    Name = "task-executor-fs-tr"
  }
}
resource "aws_security_group" "ea-task-executor-efs-sg" {
  name = "Access_Task_Executor_to_EFS"
  description = "Access from Artemis Task Executor to EFS"
  vpc_id = aws_vpc.main.id
}
resource "aws_security_group_rule" "ea-task-executor-efs-rule" {
  security_group_id = aws_security_group.ea-task-executor-efs-sg.id
  type = "ingress"
  from_port = 2049
  to_port = 2049
  protocol = "tcp"
  source_security_group_id = aws_security_group.sg_K8S.id
}
resource "aws_efs_mount_target" "efs-task-executor-mnt" {
  file_system_id = aws_efs_file_system.task-executor-fs.id
  subnet_id = aws_subnet.subnet1.id
  security_groups = [
    aws_security_group.ea-task-executor-efs-sg.id]
}

#########################################################################################################################
#                           AWS root AC                                                                                 #
#########################################################################################################################
resource "aws_s3_bucket" "EA-prod-bucket" {
  bucket = "ea-prod-bucket"
}

data "aws_iam_policy_document" "acmpca_bucket_access_prod" {
  statement {
    actions = [
      "s3:GetBucketAcl",
      "s3:GetBucketLocation",
      "s3:PutObject",
      "s3:PutObjectAcl",
    ]

    resources = [
      aws_s3_bucket.EA-prod-bucket.arn,
      "${aws_s3_bucket.EA-prod-bucket.arn}/*",
    ]

    principals {
      identifiers = [
        "acm-pca.amazonaws.com"]
      type = "Service"
    }
  }
}

resource "aws_s3_bucket_policy" "EA-bucket_policy_prod" {
  bucket = aws_s3_bucket.EA-prod-bucket.id
  policy = data.aws_iam_policy_document.acmpca_bucket_access_prod.json
}

resource "aws_acmpca_certificate_authority" "EA-prod-CA" {
  certificate_authority_configuration {
    key_algorithm = "RSA_2048"
    signing_algorithm = "SHA256WITHRSA"

    subject {
      common_name = "EA-ROOT-CA"
      country = "UK"
      locality = "London"
      organization = "Universal Asset Sciences Ltd"
      organizational_unit = "Every Asset"
    }
  }
  revocation_configuration {
    crl_configuration {
      custom_cname = "crl.dev.everyasset.internal"
      enabled = true
      expiration_in_days = 7
      s3_bucket_name = aws_s3_bucket.EA-prod-bucket.id
    }
  }
  depends_on = [
    aws_s3_bucket_policy.EA-bucket_policy_prod]

  type = "ROOT"
}
#################################################################
#                   certificates                                #
#################################################################
resource "aws_acm_certificate" "prod-notary-db-leaf" {
  domain_name       = "prod-notary-db.internal"
  #Root CA
  certificate_authority_arn = var.prod-root-CA
  tags                      = {
    "Environment" = "prod-notary-db-tr"
  }
}

resource "aws_acm_certificate" "prod-notary-node-leaf" {
  domain_name       = "prod-notary-node.internal"
  #Root CA
  certificate_authority_arn = var.prod-root-CA
  tags                      = {
    "Environment" = "prod-notary-node-tr"
  }
}
##################################################################
resource "aws_acm_certificate" "prod-oracle-db-leaf" {
  domain_name       = "prod-oracle-db.internal"
  #Root CA
  certificate_authority_arn = var.prod-root-CA
  tags                      = {
    "Environment" = "prod-oracle-db-tr"
  }
}

resource "aws_acm_certificate" "prod-oracle-node-leaf" {
  domain_name       = "prod-oracle-node.internal"
  #Root CA
  certificate_authority_arn = var.prod-root-CA
  tags                      = {
    "Environment" = "prod-oracle-node-tr"
  }
}
####################################################################
resource "aws_acm_certificate" "prod-platform-db-leaf" {
  domain_name       = "prod-platform-db.internal"
  #Root CA
  certificate_authority_arn = var.prod-root-CA
  tags                      = {
    "Environment" = "prod-platform-db-tr"
  }
}

resource "aws_acm_certificate" "prod-platform-node-leaf" {
  domain_name       = "prod-platform-node.internal"
  #Root CA
  certificate_authority_arn = var.prod-root-CA
  tags                      = {
    "Environment" = "prod-platform-node-tr"
  }
}

resource "aws_acm_certificate" "prod-k8s-cenm-db-leaf" {
  domain_name = "cenm-db.internal"
  #Root CA
  certificate_authority_arn = var.prod-root-CA
  tags = {
    "Environment" = "prod-k8s-cenm-db-tr"
  }
}
resource "aws_acm_certificate" "prod-k8s-identity-manager-leaf" {
  domain_name = "identity-manager.internal"
  #Root CA
  certificate_authority_arn = var.prod-root-CA
  tags = {
    "Environment" = "prod-k8s-identity-manager-tr"
  }
}
resource "aws_acm_certificate" "prod-k8s-network-map-leaf" {
  domain_name = "network-map.internal"
  #Root CA
  certificate_authority_arn = var.prod-root-CA
  tags = {
    "Environment" = "prod-k8s-network-map-tr"
  }
}

###########################################################################
#                 AWS CMK for Production                                  #
###########################################################################
resource "aws_kms_alias" "alias-oracle-prod" {
  name = "alias/prod_oracle_postgres_tde_master_key-tr"
  target_key_id = aws_kms_key.prod_postgres_dte_oracle.key_id
}
resource "aws_kms_key" "prod_postgres_dte_oracle" {
  description = "KMS for Postgres DTE Production"
}
############################################################################
#                 AWS Bucket for data key                                  #
############################################################################
resource "aws_s3_bucket" "EA-prod-bucket-for-data-key" {
  bucket = "ea-prod-bucket-for-data-key"
  acl = "private"

  tags = {
    Name = "ea-prod-bucket-for-data-key"
    Environment = "Production"
  }
}

##############################################################
#                AWS SSM                                     #
##############################################################
resource "aws_ssm_parameter" "PROD_NOTARY_POSTGRES_DBA_PASSWORD" {
  name = "PROD_NOTARY_POSTGRES_DBA_PASSWORD"
  type = "String"
  value = "notary_dba_pwd"
}

resource "aws_ssm_parameter" "PROD_PLATFORM_POSTGRES_DBA_PASSWORD" {
  name = "PROD_PLATFORM_POSTGRES_DBA_PASSWORD"
  type = "String"
  value = "platform_dba_pwd"
}

resource "aws_ssm_parameter" "PROD_ARTEMIS_USERNAME" {
  name = "PROD_ARTEMIS_USERNAME"
  type = "SecureString"
  value = "artemis_usr"
  overwrite = true
}
resource "aws_ssm_parameter" "PROD_ARTEMIS_PASSWORD" {
  name = "PROD_ARTEMIS_PASSWORD"
  type = "SecureString"
  value = "artemis_pwd"
  overwrite = true
}
resource "aws_ssm_parameter" "PROD_TASK_EXECUTOR_DB_USERNAME" {
  name = "PROD_TASK_EXECUTOR_DB_USERNAME"
  type = "SecureString"
  value = "task_executor_usr"
  overwrite = true
}
resource "aws_ssm_parameter" "PROD_TASK_EXECUTOR_DB_PASSWORD" {
  name = "PROD_TASK_EXECUTOR_DB_PASSWORD"
  type = "SecureString"
  value = "task_executor_pwd"
  overwrite = true
}
resource "aws_ssm_parameter" "PROD_SMTP_SERVER_HOST" {
  name = "PROD_SMTP_SERVER_HOST"
  type = "SecureString"
  value = "smtp.office365.com"
  overwrite = true
}
resource "aws_ssm_parameter" "PROD_SMTP_SERVER_USERNAME" {
  name = "PROD_SMTP_SERVER_USERNAME"
  type = "SecureString"
  value = "eawebservice@everyasset.net"
  overwrite = true
}
resource "aws_ssm_parameter" "PROD_SMTP_SERVER_PASSWORD" {
  name = "PROD_SMTP_SERVER_PASSWORD"
  type = "SecureString"
  value = "Goz11045"
  overwrite = true
}
resource "aws_ssm_parameter" "SMTP_SERVER_PORT" {
  name = "SMTP_SERVER_PORT"
  type = "SecureString"
  value = "587"
  overwrite = true
}
resource "aws_ssm_parameter" "PROD_ORACLE_POSTGRES_DBA_PASSWORD" {
  name = "PROD_ORACLE_POSTGRES_DBA_PASSWORD"
  type = "SecureString"
  value = "oracle_dba_pwd"
  overwrite = true
}

resource "aws_ssm_parameter" "PROD_ORACLE_POSTGRES_CORDA_SCHEMA" {
  name = "PROD_ORACLE_POSTGRES_CORDA_SCHEMA"
  type = "SecureString"
  value = "oracle_corda_schema"
  overwrite = true
}
resource "aws_ssm_parameter" "PROD_ORACLE_POSTGRES_CORDA_USERNAME" {
  name = "PROD_ORACLE_POSTGRES_CORDA_USERNAME"
  type = "SecureString"
  value = "prod-oracle-node.internal"
  overwrite = true
}

resource "aws_ssm_parameter" "PROD_NOTARY_POSTGRES_CORDA_SCHEMA" {
  name = "PROD_NOTARY_POSTGRES_CORDA_SCHEMA"
  type = "SecureString"
  value = "notary_corda_schema"
  overwrite = true
}

resource "aws_ssm_parameter" "PROD_NOTARY_POSTGRES_CORDA_USERNAME" {
  name = "PROD_NOTARY_POSTGRES_CORDA_USERNAME"
  type = "SecureString"
  value = "prod-notary-node.internal"
  overwrite = true
}
resource "aws_ssm_parameter" "PROD_PLATFORM_POSTGRES_CORDA_SCHEMA" {
  name = "PROD_PLATFORM_POSTGRES_CORDA_SCHEMA"
  type = "SecureString"
  value = "platform_corda_schema"
  overwrite = true
}
resource "aws_ssm_parameter" "PROD_PLATFORM_POSTGRES_CORDA_USERNAME" {
  name = "PROD_PLATFORM_POSTGRES_CORDA_USERNAME"
  type = "SecureString"
  value = "prod-platform-node.internal"
  overwrite = true
}
