#!/bin/sh
{{ if eq .Values.bashDebug true }}
set -x
{{ end }}

#
# main run
#
if [ -f {{ .Values.jarPath }}/corda.jar ]
then
{{ if eq .Values.bashDebug true }}
    sha256sum {{ .Values.jarPath }}/corda.jar 
{{ end }}
    echo
    echo "Corda: starting Corda node ..."
    echo
    java -jar {{ .Values.jarPath }}/corda.jar -f {{ .Values.configPath }}/node.conf
    EXIT_CODE=${?}
else
    echo "Missing corda jar file in {{ .Values.jarPath }} folder:"
    ls -al {{ .Values.jarPath }}
    EXIT_CODE=110
fi

if [ "${EXIT_CODE}" -ne "0" ]
then
    HOW_LONG={{ .Values.sleepTimeAfterError }}
    echo
    echo "Corda Node startup failed - exit code: ${EXIT_CODE} (error)"
    echo
    echo "Going to sleep for requested ${HOW_LONG} seconds to let you login and investigate."
    echo
    sleep ${HOW_LONG}
fi

echo