CREATE USER "identity-manager.internal";
CREATE SCHEMA "identity_manager_schema";
GRANT USAGE, CREATE ON SCHEMA "identity_manager_schema" TO "identity-manager.internal";
GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES ON ALL tables IN SCHEMA "identity_manager_schema" TO "identity-manager.internal";
ALTER DEFAULT privileges IN SCHEMA "identity_manager_schema" GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES ON tables TO "identity-manager.internal";
GRANT USAGE, SELECT ON ALL sequences IN SCHEMA "identity_manager_schema" TO "identity-manager.internal";
ALTER DEFAULT privileges IN SCHEMA "identity_manager_schema" GRANT USAGE, SELECT ON sequences TO "identity-manager.internal";
ALTER ROLE "identity-manager.internal" SET search_path = "identity_manager_schema";

CREATE USER "network-map.internal";
CREATE SCHEMA "network_map_schema";
GRANT USAGE, CREATE ON SCHEMA "network_map_schema" TO "network-map.internal";
GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES ON ALL tables IN SCHEMA "network_map_schema" TO "network-map.internal";
ALTER DEFAULT privileges IN SCHEMA "network_map_schema" GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES ON tables TO "network-map.internal";
GRANT USAGE, SELECT ON ALL sequences IN SCHEMA "network_map_schema" TO "network-map.internal";
ALTER DEFAULT privileges IN SCHEMA "network_map_schema" GRANT USAGE, SELECT ON sequences TO "network-map.internal";
ALTER ROLE "network-map.internal" SET search_path = "network_map_schema";
