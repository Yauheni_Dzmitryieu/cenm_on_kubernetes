---
- hosts:
    - master
  tasks:
  - name: AWS configure
    shell: |
      aws configure set aws_access_key_id ${AWS_ACCESS_KEY_ID} \
      &&   aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY} \
      &&   aws configure set default.region ${AWS_DEFAULT_REGION}
    environment:
      AWS_DEFAULT_REGION: "eu-west-2"
      AWS_ACCESS_KEY_ID: {{ "AWS_ACCESS_KEY_ID" }}
      AWS_SECRET_ACCESS_KEY: {{ "AWS_SECRET_ACCESS_KEY" }}
  - name: Make sure /tmp/certs directory exist
    file: path={{ item }} state=directory mode=0775
    with_items:
      - "/tmp/certs"
  - name: Check that the root-ca.crt exists
    stat:
      path: /tmp/certs/root-ca.crt
    register: root_crt_stat
    become: yes

  - name: Check that the root-ca.crt exists
    stat:
      path: /tmp/certs/root-ca.crt
    register: root_crt_stat
  - name: Get Root CA
    shell: |
      cd /tmp/certs \
      && aws acm-pca get-certificate-authority-certificate \
         --certificate-authority-arn \
         arn:aws:acm-pca:eu-west-2:400712362531:certificate-authority/7606f157-d4ea-45ac-badb-34e14bfc8ade \
         --output text > root-ca.crt
    when: not root_crt_stat.stat.exists or root_crt_stat.stat.size == 0

  - name: Change root CA ownership
    file:
      path: /tmp/certs/root-ca.crt
      owner: '0'
      group: '70'
    become: yes
    when: not root_crt_stat.stat.exists

  - name: Download identity-manager certificates and keys
    shell: >
      cd /tmp/certs \
      && export randomPassphrase=$(openssl rand -base64 7) \
      && aws acm export-certificate \
          --certificate-arn arn:aws:acm:eu-west-2:400712362531:certificate/730f64f7-d0b3-43b7-a560-0fc3f5764f02 \
          --passphrase $randomPassphrase > identity-manager.aws_cert \
      && jq -r '.Certificate' identity-manager.aws_cert > identity-manager.crt \
      && jq -r '.PrivateKey' identity-manager.aws_cert > identity-manager.encrypted.key \
      && openssl pkcs8 -topk8 -inform PEM -in identity-manager.encrypted.key -outform DER -nocrypt -out identity-manager.pk8 -passin pass:$randomPassphrase \
      && rm -f *.encrypted.key identity-manager.aws_cert
    register: export_crt_cmd
    changed_when: export_crt_cmd.rc == 0

  - name: Download network-map certificates and keys
    shell: >
      cd /tmp/certs \
      && export randomPassphrase=$(openssl rand -base64 7) \
      && aws acm export-certificate \
          --certificate-arn arn:aws:acm:eu-west-2:400712362531:certificate/8a941ac3-4767-44ef-a47b-b9b5cea5a79e \
          --passphrase $randomPassphrase > network-map.aws_cert \
      && jq -r '.Certificate' network-map.aws_cert > network-map.crt \
      && jq -r '.PrivateKey' network-map.aws_cert > network-map.encrypted.key \
      && openssl pkcs8 -topk8 -inform PEM -in network-map.encrypted.key -outform DER -nocrypt -out network-map.pk8 -passin pass:$randomPassphrase \
      && rm -f *.encrypted.key network-map.aws_cert
    register: export_crt_cmd
    changed_when: export_crt_cmd.rc == 0

  - name: Download cenm-db certificates and keys
    shell: >
      cd /tmp/certs \
      && export randomPassphrase=$(openssl rand -base64 7) \
      && aws acm export-certificate \
        --certificate-arn arn:aws:acm:eu-west-2:400712362531:certificate/a8b886d3-b2ca-4a23-ad42-9baacf771c00 \
        --passphrase $randomPassphrase > cenm-db.aws_cert \
      && jq -r '.Certificate' cenm-db.aws_cert > cenm-db.crt \
      && jq -r '.PrivateKey' cenm-db.aws_cert > cenm-db.encrypted.key \
      && openssl rsa -in cenm-db.encrypted.key -out cenm-db.key -passin pass:$randomPassphrase \
      && rm -f *.encrypted.key cenm-db.aws_cert
    register: export_crt_cmd
    changed_when: export_crt_cmd.rc == 0
###############################################################################################################
  - name: Download Corda Node Platform certificates and keys
    shell: >
      cd /tmp/certs \
        && export randomPassphrase=$(openssl rand -base64 7) \
        && aws acm export-certificate \
          --certificate-arn arn:aws:acm:eu-west-2:400712362531:certificate/8950e710-6ae2-4b2c-8a42-1a1596b9f30b \
          --passphrase $randomPassphrase > platform-node.aws_cert \
        && jq -r '.Certificate' platform-node.aws_cert > platform-node.crt \
        && jq -r '.PrivateKey' platform-node.aws_cert > platform-node.encrypted.key \
        && openssl pkcs8 -topk8 -inform PEM -in platform-node.encrypted.key -outform DER -nocrypt -out platform-node.pk8 -passin pass:$randomPassphrase \
        && rm -f *.encrypted.key platform-node.aws_cert
    register: export_crt_cmd
    changed_when: export_crt_cmd.rc == 0
  - name: Download Postgres certificates and keys
    shell: >
      cd /tmp/certs \
        && export randomPassphrase=$(openssl rand -base64 7) \
        && aws acm export-certificate \
          --certificate-arn arn:aws:acm:eu-west-2:400712362531:certificate/a3b7a62b-8eac-43d6-b229-a98f22d045d0 \
          --passphrase $randomPassphrase > platform-node-db.aws_cert \
        && jq -r '.Certificate' platform-node-db.aws_cert > platform-node-db.crt \
        && jq -r '.PrivateKey' platform-node-db.aws_cert > platform-node-db.encrypted.key \
        && openssl rsa -in platform-node-db.encrypted.key -out platform-node-db.key -passin pass:$randomPassphrase \
        && rm -f *.encrypted.key platform-node-db.aws_cert
    register: export_crt_cmd
    changed_when: export_crt_cmd.rc == 0
####################################################################################################################
  - name: Download Corda Node Notary certificates and keys
    shell: >
      cd /tmp/certs \
      && export randomPassphrase=$(openssl rand -base64 7) \
      && aws acm export-certificate \
        --certificate-arn arn:aws:acm:eu-west-2:400712362531:certificate/76ba9e62-4728-48f4-8bca-4d34c01756b2 \
        --passphrase $randomPassphrase > notary-node.aws_cert \
      && jq -r '.Certificate' notary-node.aws_cert > notary-node.crt \
      && jq -r '.PrivateKey' notary-node.aws_cert > notary-node.encrypted.key \
      && openssl pkcs8 -topk8 -inform PEM -in notary-node.encrypted.key -outform DER -nocrypt -out notary-node.pk8 -passin pass:$randomPassphrase \
      && rm -f *.encrypted.key notary-node.aws_cert
    register: export_crt_cmd
    changed_when: export_crt_cmd.rc == 0
  - name: Download Postgres certificates and keys
    shell: >
      cd /tmp/certs \
      && export randomPassphrase=$(openssl rand -base64 7) \
      && aws acm export-certificate \
        --certificate-arn arn:aws:acm:eu-west-2:400712362531:certificate/8d2f8c88-2e54-40f4-896a-0d2dab99e118 \
        --passphrase $randomPassphrase > notary-node-db.aws_cert \
      && jq -r '.Certificate' notary-node-db.aws_cert > notary-node-db.crt \
      && jq -r '.PrivateKey' notary-node-db.aws_cert > notary-node-db.encrypted.key \
      && openssl rsa -in notary-node-db.encrypted.key -out notary-node-db.key -passin pass:$randomPassphrase \
      && rm -f *.encrypted.key notary-node-db.aws_cert
    register: export_crt_cmd
    changed_when: export_crt_cmd.rc == 0
########################################################################################################################
  - name: Download Corda Node Oracle certificates and keys
    shell: >
      cd /tmp/certs \
      && export randomPassphrase=$(openssl rand -base64 7) \
      && aws acm export-certificate \
        --certificate-arn arn:aws:acm:eu-west-2:400712362531:certificate/1d7eb3a3-89fb-476a-bd88-7f0826e3a252 \
        --passphrase $randomPassphrase > oracle-node.aws_cert \
      && jq -r '.Certificate' oracle-node.aws_cert > oracle-node.crt \
      && jq -r '.PrivateKey' oracle-node.aws_cert > oracle-node.encrypted.key \
      && openssl pkcs8 -topk8 -inform PEM -in oracle-node.encrypted.key -outform DER -nocrypt -out oracle-node.pk8 -passin pass:$randomPassphrase \
      && rm -f *.encrypted.key oracle-node.aws_cert
    register: export_crt_cmd
    changed_when: export_crt_cmd.rc == 0
  - name: Download Postgres certificates and keys
    shell: >
      cd /tmp/certs \
      && export randomPassphrase=$(openssl rand -base64 7) \
      && aws acm export-certificate \
        --certificate-arn arn:aws:acm:eu-west-2:400712362531:certificate/f797c985-5e46-401e-8c85-0ff67868e93e \
        --passphrase $randomPassphrase > oracle-node-db.aws_cert \
      && jq -r '.Certificate' oracle-node-db.aws_cert > oracle-node-db.crt \
      && jq -r '.PrivateKey' oracle-node-db.aws_cert > oracle-node-db.encrypted.key \
      && openssl rsa -in oracle-node-db.encrypted.key -out oracle-node-db.key -passin pass:$randomPassphrase \
      && rm -f *.encrypted.key oracle-db.aws_cert
    register: export_crt_cmd
    changed_when: export_crt_cmd.rc == 0