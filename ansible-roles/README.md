# Deploy Kubernetes
Use this link https://blog.scottlowe.org/2019/07/30/adding-a-name-to-kubernetes-api-server-certificate/
for ssh tuneling from host to API k8s.
[Playbook for prepare nodes](../amsible-roles/initial.yml)

For run need use command:
``` bash
ansible-playbook initial.yml -i hosts-k8s.yml
```

[Playbook for prepare master server](../amsible-roles/master.yml)

For run need use command:
``` bash
ansible-playbook master.yml -i hosts-k8s.yml
```

[Playbook for prepare worker servers](../amsible-roles/workers.yml)

For run need use command:
``` bash
ansible-playbook workers.yml -i hosts-k8s.yml
```